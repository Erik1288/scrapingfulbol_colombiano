from django.db import models

'''model in which the information of each of the names of the teams (Colombian soccer)
   and their positions are saved'''


class PositionsTable(models.Model):
    name = models.CharField(max_length=50)
    positions = models.CharField(max_length=100)
