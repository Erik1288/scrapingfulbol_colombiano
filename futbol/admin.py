from django.contrib import admin

from futbol.models import PositionsTable

'''registration of our model, in the django platform, for admin users'''
admin.site.register(PositionsTable)
