from django.urls import path, include
from futbol import views
from rest_framework import routers
from django.contrib import admin

'''connecting the url of our API, with the url of our project'''
router = routers.DefaultRouter()
router.register(r'positions', views.PositionsTableViewSet)

'''defining each of the urls with which we show the information on the screen'''
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api-futbol/', include('rest_framework.urls', namespace='rest_framework')),
]
